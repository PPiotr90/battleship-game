package pl.pp.battleship;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class BattleshipServer {
    boolean firsPlayerReady = false;
    boolean secondPlayerReady = false;
    private ServerSocket serverSocket;
    private TcpConnection firstPlayerConnection;
    private TcpConnection secondPlayerConnection;
    private List<TcpConnection> clientsList = new ArrayList<>();
    private int turnOfPlayer;

    public BattleshipServer(int i) throws IOException {
        serverSocket = new ServerSocket(i);
        turnOfPlayer = 1;

    }

    public void startServer() throws IOException {
        while (!(serverSocket.isClosed())) {

            if (firstPlayerConnection == null) {
                Socket socket = serverSocket.accept();
                firstPlayerConnection = createClientConnection(socket, 1);
                clientsList.add(firstPlayerConnection);
                firstPlayerConnection.sendMessage("!waiting for players");
                firstPlayerConnection.sendMessage("you play as: 1 player");
                firstPlayerConnection.startListener();
                System.out.println("podłączono 1. gracza");

            } else if (secondPlayerConnection == null) {
                Socket socket = serverSocket.accept();
                secondPlayerConnection = createClientConnection(socket, 2);

                clientsList.add(secondPlayerConnection);
                secondPlayerConnection.sendMessage("!waiting for players");
                secondPlayerConnection.sendMessage("you play as: 2 player");
                secondPlayerConnection.startListener();
                System.out.println("podłączono 2. gracza");
            }

        }
    }

    private TcpConnection createClientConnection(Socket socket, int player) throws IOException {
        TcpConnection result = new TcpConnection(socket);
        result.setOnMessageReceived(message -> {
            System.out.println(message);
            if (player == 1) {
                if (message.equals("ready")) {
                    System.out.println("1. ready");
                    firsPlayerReady = true;
                } else if (message.equals("?")) {
                    firstPlayerConnection.sendMessage("?" + String.valueOf(1));
                } else if (turnOfPlayer == 1) {
                    secondPlayerConnection.sendMessage(message);
                    if (message.startsWith("#")) nextPlayer();
                    if (message.endsWith("#")) nextPlayer();
                } else if (message.startsWith("#")) firstPlayerConnection.sendMessage("It is not your turn!!");
            } else if (message.equals("ready")) {
                System.out.println("2. ready");
                secondPlayerReady = true;
            } else if (message.equals("?")) {
                secondPlayerConnection.sendMessage("?" + String.valueOf(2));
            } else if (turnOfPlayer == 2) {
                firstPlayerConnection.sendMessage(message);
                if (message.startsWith("#")) nextPlayer();
                if (message.endsWith("#")) nextPlayer();
            } else if (message.startsWith("#"))
                secondPlayerConnection.sendMessage("It is not your turn!!");

            for (TcpConnection tcpConnection : clientsList) {
                if (firsPlayerReady && secondPlayerReady)
                    tcpConnection.sendMessage("!Turn of player: " + String.valueOf(turnOfPlayer));
                tcpConnection.sendMessage("P" + String.valueOf(turnOfPlayer));
            }

        });
        return result;
    }

    private void nextPlayer() {
        if (turnOfPlayer == 1) turnOfPlayer = 2;
        else if (turnOfPlayer == 2) turnOfPlayer = 1;
        for (TcpConnection tcpConnection : clientsList) {
            tcpConnection.sendMessage("!Turn of player: " + String.valueOf(turnOfPlayer));
            tcpConnection.sendMessage("P" + String.valueOf(turnOfPlayer));
        }
    }
}
