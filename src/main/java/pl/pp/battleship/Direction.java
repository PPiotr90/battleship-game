package pl.pp.battleship;

public enum Direction {
    VERTICAL, HORIZONTAL
}
