package pl.pp.battleship;

import java.io.IOException;

public class MainServer {
    public static void main(String[] args) throws IOException {
        BattleshipServer battleshipServer = new BattleshipServer(12345);
        battleshipServer.startServer();
    }
}
