package pl.pp.battleship;

public enum AfterShot {
    MISHIT, HUNTED, HUNTED_AND_DROPPED
}
