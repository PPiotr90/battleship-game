package pl.pp.battleship;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.function.Consumer;

public class TcpConnection {
    Scanner scanner;
    private PrintWriter printWriter;
    private Consumer<String> onMessageReceived;

    public TcpConnection(Socket socket) throws IOException {
        this.printWriter = new PrintWriter(socket.getOutputStream(), true);
        this.scanner = new Scanner(socket.getInputStream());
    }

    public void setOnMessageReceived(Consumer<String> onMessageReceived) {
        this.onMessageReceived = onMessageReceived;
    }

    public void sendMessage(String message) {
        printWriter.println(message);
    }
    public String readMessage() {
        return scanner.nextLine();
    }

    public void startListener() {
        Thread thread = new Thread(() -> {
            while (!Thread.interrupted()) {
                String message = readMessage();
                onMessageReceived.accept(message);
            }
        });
        thread.setDaemon(true);
        thread.start();
    }


}