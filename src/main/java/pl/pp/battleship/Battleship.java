package pl.pp.battleship;

import java.util.Arrays;
import java.util.HashMap;

public class Battleship {
    Ship[] ships;
    private int[][] localSea;
    private int[][] enemySea;
    private int BOUNDS = 10;
    private HashMap<Point, AfterShot> attackPoints;
    private HashMap<Point, AfterShot> defeatPoints;
    private Presentation presentation;


    public Battleship() {
        ships = new Ship[5];
        localSea = new int[BOUNDS][BOUNDS];
        enemySea = new int[BOUNDS][BOUNDS];
        attackPoints = new HashMap<>();
        defeatPoints = new HashMap<>();
    }

    public void add(Ship ship, int i) {

        if (!(isContact(ship))) {
            ships[i] = ship;
        }
    }


    private boolean isContact(Ship newShip) {
        for (Ship existedShip : ships) {
            if (existedShip != null) {
                for (Point existedMast : existedShip.getMasts()) {
                    int x = existedMast.getX();
                    int y = existedMast.getY();
                    for (Point newMast : newShip.getMasts()) {
                        for (int i = -1; i <= 1; i++) {
                            for (int j = -1; j <= 1; j++) {
                                if (new Point(x + i, y + j).equals(newMast)
                                        || newMast.getX() > BOUNDS || newMast.getX() < 0
                                        || newMast.getY() > BOUNDS || newMast.getY() < 0) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public AfterShot shotTo(Point point) {
        AfterShot result = AfterShot.MISHIT;
        for (int i = 0; i < ships.length; i++) {
            if (ships[i] != null) {
                if ((ships[i].isStruck(point))) {
                    result = AfterShot.HUNTED;
                }
                if (!(Arrays.stream(ships[i].getMasts())
                        .filter(mast -> mast != null)
                        .findAny().isPresent())) {
                    ships[i] = null;
                    result = AfterShot.HUNTED_AND_DROPPED;
                }
            }

        }
        defeatPoints.put(point, result);
        return result;
    }

    public void addAttack(Point point, AfterShot afterShot) {

        attackPoints.put(point, afterShot);
        if (afterShot == AfterShot.HUNTED_AND_DROPPED) {
            changeHuntedNeighborsOnDropped(point, attackPoints);
        }
    }

    public void addDefence(Point point, AfterShot afterShot) {
        this.defeatPoints.put(point, afterShot);
        if (afterShot == AfterShot.HUNTED_AND_DROPPED) {
            changeHuntedNeighborsOnDropped(point, defeatPoints);
        }
    }

    private void changeHuntedNeighborsOnDropped(Point point, HashMap<Point, AfterShot> hashMap) {
        int x = point.getX();
        int y = point.getY();
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                Point neighbor = new Point(x + i, y + j);
                if (hashMap.containsKey(neighbor)) {
                    if (hashMap.get(neighbor) == AfterShot.HUNTED) {
                        hashMap.replace(neighbor, AfterShot.HUNTED, AfterShot.HUNTED_AND_DROPPED);
                        changeHuntedNeighborsOnDropped(neighbor, hashMap);
                    }
                }

            }

        }
    }

    public Battleship setPresentation(Presentation presentation) {
        this.presentation = presentation;
        return this;
    }

    public void show() {
        if (presentation != null) {
            presentation.show();
        }
    }

    public Ship[] getShips() {
        return ships;
    }

    public void deleteShip(int i) {
        ships[i] = null;
    }

    public HashMap<Point, AfterShot> getAttackPoints() {
        return attackPoints;
    }

    public HashMap<Point, AfterShot> getDefeatPoints() {
        return defeatPoints;
    }
}

