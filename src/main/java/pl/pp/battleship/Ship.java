package pl.pp.battleship;

import java.util.concurrent.atomic.AtomicInteger;

public class Ship {
    private Point[] masts;

    public  Ship(Point bow, Integer length, Direction direction){
        Point[] points=new  Point[length];
        int x = bow.getX();
        int y = bow.getY();
        for (int i = 0; i < length; i++) {
            switch (direction) {
                case HORIZONTAL:
                    points[i]= new  Point(x+i, y);
                    break;
                case VERTICAL:
                    points[i]=new Point(x, y+i);
                    break;

            }

        }

        this.masts = points;
    }

    public boolean isStruck(Point point) {
        for (int i = 0; i < masts.length; i++) {
            if (masts[i] != null) {
                if (masts[i].equals(point)) {
                    masts[i]= null;
                    return true;
                }
            }
        }

        return false;
    }

    public Point[] getMasts() {
        return masts;
    }
}
