package pl.pp.battleship.ui;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import pl.pp.battleship.*;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class Controller {
    Button[][] localButtons;
    Button[][] enemyButtons;
    Button[] shipButtons;
    Label[][] localLabels;
    Label[][] enemyLabels;
    int numberOfPlayer;
    int turnOf;
    Ship newShip;
    TcpConnection tcpConnection;
    Button[] cancelledButtons;
    Battleship battleship;
    @FXML
    RadioButton vRadioButton;
    @FXML
    RadioButton hRadioButton;
    @FXML
    Button showButton;
    @FXML
    Button startButton;
    @FXML
    Label messageLabel;
    @FXML
    Label playerLabel;
    @FXML
    private GridPane localGridPane;
    @FXML
    private GridPane enemyGridPane;
    @FXML
    private Button smallShipButton;
    @FXML
    private Button mediumShip1Button;
    @FXML
    private Button mediumShip2Button;
    @FXML
    private Button mediumShip3Button;
    @FXML
    private Button largeShipButton;

    public void initialize() throws IOException {
        tcpConnection = new TcpConnection(new Socket("localhost", 12345));
        this.setTcpMessage();
        tcpConnection.startListener();
        tcpConnection.sendMessage("?");
        startButton.setVisible(false);
        cancelledButtons = new Button[5];
        battleship = new Battleship();
        shipButtons = new Button[5];
        shipButtons[0] = smallShipButton;
        shipButtons[1] = mediumShip1Button;
        shipButtons[2] = mediumShip2Button;
        shipButtons[3] = mediumShip3Button;
        shipButtons[4] = largeShipButton;
        localButtons = new Button[11][11];
        localGridPane.setAlignment(Pos.CENTER);
        enemyGridPane.setAlignment(Pos.CENTER);
        enemyButtons = new Button[11][11];
        localLabels = new Label[11][11];
        enemyLabels = new Label[11][11];
        showButton.setOnAction(event -> battleship.show());
        localGridPane.setAlignment(Pos.CENTER);
        enemyGridPane.setAlignment(Pos.CENTER);
        localGridPane.setPrefSize(270, 270);
        enemyGridPane.setPrefSize(270, 270);

        for (int i = 1; i < 11; i++) {
            char c = (char) (i + 64);
            Text verticalLabel = new Text(" " + String.valueOf(i) + " ");
            Text verticalLabel2 = new Text(" " + String.valueOf(i) + " ");

            Label horizontalLabel = new Label(" " + String.valueOf(c) + " ");
            Label horizontalLabel2 = new Label(" " + String.valueOf(c) + " ");
            horizontalLabel2.setAlignment(Pos.CENTER);
            horizontalLabel.setAlignment(Pos.CENTER);

            enemyGridPane.add(verticalLabel, 0, i);
            localGridPane.add(verticalLabel2, 0, i);
            enemyGridPane.add(horizontalLabel, i, 0);
            localGridPane.add(horizontalLabel2, i, 0);
        }

        for (int i = 1; i < 11; i++) {
            for (int j = 1; j < 11; j++) {
                Label localLabel = new Label();
                localLabel.setPrefSize(20, 20);
                Label enemyLabel = new Label();
                enemyLabel.setPrefSize(20, 20);
                Button localButton = new Button();
                Button enemyButton = new Button();
                int finalI = i;
                int finalJ = j;
                String messageToAttack = new String("#");
                messageToAttack += String.valueOf(finalI) + "," + String.valueOf(finalJ);
                String finalMessageToAttack = messageToAttack;
                enemyButton.setOnAction(event -> {

                    tcpConnection.sendMessage(finalMessageToAttack);
                    tcpConnection.sendMessage("Shout to: " + finalMessageToAttack.substring(1));
                    if (numberOfPlayer == turnOf) enemyButton.setVisible(false);

                });
                localButton.setPrefSize(20, 20);
                localButton.setBackground(new Background(new BackgroundFill(Color.BLUE, CornerRadii.EMPTY, Insets.EMPTY)));
                localLabel.setAlignment(Pos.CENTER);
                enemyButton.setPrefSize(20, 20);
                enemyButton.setBackground(new Background(new BackgroundFill(Color.BLUE, CornerRadii.EMPTY, Insets.EMPTY)));
                localButtons[i][j] = localButton;
                localLabels[i][j] = localLabel;
                localGridPane.add(localLabel, i, j);
                localGridPane.add(localButton, i, j);
                enemyButton.setVisible(false);
                enemyButtons[i][j] = enemyButton;
                enemyLabel.setAlignment(Pos.CENTER);
                enemyLabels[i][j] = enemyLabel;
                enemyGridPane.add(enemyLabel, i, j);
                enemyGridPane.add(enemyButton, i, j);
            }
        }


        enemyGridPane.autosize();
        localGridPane.autosize();

        ToggleGroup group = new ToggleGroup();
        vRadioButton.setToggleGroup(group);
        hRadioButton.setToggleGroup(group);

        for (int i = 1; i < 4; i++) {
            int finalI = i;
            int finalI1 = i;
            shipButtons[i].setOnAction(event -> {
                putShipOn(3, finalI1);

            });
        }
        shipButtons[0].setOnAction(event -> {
            putShipOn(2, 0);
        });
        shipButtons[4].setOnAction(event -> {
            putShipOn(4, 4);

        });
        enemyGridPane.setGridLinesVisible(true);
        localGridPane.setGridLinesVisible(true);
        this.setPresentation();

    }

    private void disableButtonsUnderShips() {
        Ship[] ships = battleship.getShips();
        for (Ship existedShip : ships) {
            if (existedShip != null) {
                for (Point mast : existedShip.getMasts()) {
                    int x = mast.getX();
                    int y = mast.getY();
                    localButtons[x][y].setVisible(false);
                }
            }
        }
    }

    private void putShipOn(int size, int i) {
        for (int x = 1; x < 11; x++) {
            for (int y = 1; y < 11; y++) {
                int finalY = y;
                int finalX = x;
                localButtons[x][y].setOnAction(event1 -> {
                    Point bow = new Point(finalX, finalY);
                    Direction direction = setDirection();
                    newShip = new Ship(bow, size, direction);
                    battleship.add(newShip, i);

                    if (battleship.getShips()[i] != null) {
                        shipButtons[i].setVisible(false);
                        this.disableButtonsUnderShips();
                        this.addCanceledButtonToShip(i, bow);
                        newShip = null;
                    }
                    setStartButton();
                    battleship.show();
                });
            }
        }
    }

    private Direction setDirection() {
        if (vRadioButton.isSelected()) {
            return Direction.VERTICAL;
        }
        return Direction.HORIZONTAL;
    }

    private void addCanceledButtonToShip(int i, Point bow) {
        Button button = new Button("X");
        button.resize(2, 2);

        button.setStyle("-fx-text-fill: red; -fx-padding:0; -fx-font-size: 10");

        button.setOnAction(event -> {
            for (Point mast : battleship.getShips()[i].getMasts()) {
                int x = mast.getX();
                int y = mast.getY();
                localButtons[x][y].setVisible(true);
            }
            shipButtons[i].setVisible(true);
            battleship.deleteShip(i);
            button.setVisible(false);
            battleship.show();

        });
        cancelledButtons[i] = button;
        localGridPane.add(button, bow.getX(), bow.getY());
    }

    private void setPresentation() {
        battleship.setPresentation(() -> {
            for (Ship ship : battleship.getShips()) {
                if (ship != null) {
                    for (Point mast : ship.getMasts()) {
                        if (mast != null) {
                            int x = mast.getX();
                            int y = mast.getY();
                            localLabels[x][y].setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
                        }
                    }
                }
            }
            showMapOnSea(battleship.getAttackPoints(), enemyLabels);
            showMapOnSea(battleship.getDefeatPoints(), localLabels);
        });
    }


    private void setStartButton() {
        if (battleship.getShips()[0] != null && battleship.getShips()[1] != null
                && battleship.getShips()[2] != null && battleship.getShips()[3] != null && battleship.getShips()[4] != null) {
            startButton.setVisible(true);
            startButton.setOnAction(event -> {
                for (int x = 1; x < 11; x++) {
                    for (int y = 1; y < 11; y++) {
                        localButtons[x][y].setVisible(false);
                        enemyButtons[x][y].setVisible(true);

                    }

                }
                for (Button button : cancelledButtons) {
                    button.setVisible(false);

                }
                startButton.setVisible(false);
                tcpConnection.sendMessage("ready");
            });
        }
    }

    private void showMapOnSea(HashMap<Point, AfterShot> map, Label[][] labels) {
        for (Map.Entry<Point, AfterShot> position : map.entrySet()) {
            int x = position.getKey().getX();
            int y = position.getKey().getY();
            switch (position.getValue()) {
                case MISHIT:
                    labels[x][y].setStyle("-fx-text-fill: blue");
                    labels[x][y].setText("O");
                    break;
                case HUNTED:
                    labels[x][y].setBackground(new Background(new BackgroundFill(Color.GRAY, CornerRadii.EMPTY, Insets.EMPTY)));
                    labels[x][y].setStyle("-fx-text-fill: red");
                    labels[x][y].setText("X");
                    break;
                case HUNTED_AND_DROPPED:
                    labels[x][y].setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
                    break;
            }
        }
    }

    private void setTcpMessage() {
        tcpConnection.setOnMessageReceived(message -> {
            if (message.startsWith("#")) {
                message = message.substring(1);
                String[] coordinates = message.split(",");
                int x = Integer.parseInt(coordinates[0]);
                int y = Integer.parseInt(coordinates[1]);
                Point point = new Point(x, y);
                AfterShot effect = battleship.shotTo(point);
                String answer;
                if (allShipsAreDead()) {
                    answer = "WINNER";
                    Platform.runLater(() -> messageLabel.setText("You loose"));
                } else answer = String.valueOf(effect);
                battleship.addDefence(point, effect);


                if (effect == AfterShot.HUNTED || effect == AfterShot.HUNTED_AND_DROPPED)
                    tcpConnection.sendMessage("*" + answer + "," + message + ",#");
                else tcpConnection.sendMessage("*" + answer + "," + message);
                tcpConnection.sendMessage(String.valueOf(effect).replace("_", " "));
            } else if (message.startsWith("*")) {
                message = message.substring(1);
                String[] values = message.split(",");
                AfterShot afterShot;
                switch (values[0]) {
                    case "MISHIT":
                        afterShot = AfterShot.MISHIT;
                        break;
                    case "HUNTED":
                        afterShot = AfterShot.HUNTED;
                        break;

                    case "HUNTED_AND_DROPPED":
                        afterShot = AfterShot.HUNTED_AND_DROPPED;
                        break;
                    default:
                        afterShot = AfterShot.MISHIT;

                }
                int x = Integer.parseInt(values[1]);
                int y = Integer.parseInt(values[2]);
                Point point = new Point(x, y);
                battleship.addAttack(point, afterShot);
                if (message.contains("WINNER")) Platform.runLater(() -> messageLabel.setText("Winner!!"));
                else Platform.runLater(() -> messageLabel.setText(String.valueOf(afterShot)));
            } else if (message.startsWith("!")) {
                String finalMessage1 = message;
                Platform.runLater(() -> playerLabel.setText(finalMessage1.substring(1)));
            } else if (message.startsWith("?")) {
                numberOfPlayer = Integer.valueOf(message.substring(1));
            } else if ((message.startsWith("P"))) {
                turnOf = Integer.valueOf(message.substring(1));

            } else {
                String finalMessage = message;
                Platform.runLater(() -> messageLabel.setText(finalMessage));
            }
            Platform.runLater(() -> battleship.show());
        });
    }

    private boolean allShipsAreDead() {
        for (Ship ship : battleship.getShips()) {
            if (ship != null) {
                return false;
            }
        }
        return true;
    }
}

